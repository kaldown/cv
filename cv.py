#!/usr/bin/python
# -*- coding: utf-8 -*-

from requests import get, Session
from collections import Counter
from lxml import html, etree
from pymongo import MongoClient
import sys

import decrypt as d

keys_to_check = (u'Условия', u'Тип работы', u'График работы', u'Контактное лицо', 
                 u'Обязанности', 'E-mail', u'Адрес', u'Телефон', u'Требования')

def stripper(STRING):
    # using just to clear from whitespaces
    res = STRING.replace('\t', ' ').strip()
    return res

class CV(object):

    def auth_72(self):
        auth = d.decrypt()['72']
        with Session() as s:
            data = {'url': 'http://72.ru',
                    'password': auth['password'],
                    'email': auth['user']}
            
            r = s.post('https://loginka.ru/auth/', data=data)
            token = r.url[r.url.index('token=')+6:]
            url = 'http://72.ru/put_token_to_user/?token=' + token + '&dummy_put_token_to_user=yes'
            headers = {'X-Requested-With': 'XMLHttpRequest', 'Referer': r.url}
            s.get(url, headers=headers)
        return s
    
    def auth_hh(self):

    def __init__(self, url, auth, db=None, session=None):
        self.url = url
        self.auth = auth
        self.db = MongoClient().cv
        self.session = session

    def target2(self):
        s = self.session
        r = s.get('http://72.ru/job/favorite/vacancy/')
        p = html.fromstring(r.content)
        tree = p.xpath('//body//table[@class="table2"]/tbody/tr')
        result = {p.attrib['id']:p for p in tree}
        return result

    def get_document(self):
        # returns a dictionary of expected elements
        elements = self.target()
        result = {stripper(k.text_content()):
                  stripper(k.getnext().text_content()) for k in elements
                  if k.text_content() in keys_to_check}
        return result

    def mongo_insert(self):
        document = self.get_document()
        result = self.db.seventy_two.update(document, document, upsert=True)
        return result
    
