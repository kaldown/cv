#!/usr/bin/python

from os.path import expanduser, exists
from subprocess import check_output
from json import loads

def decrypt():
    try:
        assert exists(expanduser("~/.gnupg/"))
    except AssertionError:
        exit(1)
    result = check_output("gpg -q -u kaldown_cv -d auth", shell=True)
    result = loads(result.decode('utf-8'))

    return result

