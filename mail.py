#!/usr/bin/python

import smtplib
from email.mime.text import MIMEText

From = 'FROM'
To = 'TO'
Subject = 'Subject: Title of the message'
msg = 'this is my message!'
username = 'USERNAME'
password = 'PASSWD'

msg = '\r\n'.join(['From: ' + From, 
                    'To: ' + To, 
                    Subject, 
                    '', 
                    msg, 
                    '\n'])

print(msg)

s = smtplib.SMTP('smtp.gmail.com', 587)
s.ehlo()
s.starttls()
s.login(username, password)
s.sendmail(From, To, msg)
s.quit()

