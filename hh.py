from requests import get, Session, post
from lxml import html

with Session() as s:
    headers = {"user-agent": "CV (kaldownb@gmail.com)"}

    s = post('https://tyumen.hh.ru/account/login', 
            allow_redirects=True,
            headers=headers)
